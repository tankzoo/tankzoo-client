import { EventBus } from '../EventBus';
import { Scene, Math } from 'phaser';
import Tank from '../model/Tank';
import { log } from 'console';

export class Game extends Scene
{
    camera: Phaser.Cameras.Scene2D.Camera;
    background: Phaser.GameObjects.Image;
    gameText: Phaser.GameObjects.Text;
    player: Phaser.GameObjects.Sprite;
    players: Array<Phaser.GameObjects.Sprite> = [];

    //layers
    playerLayer: Phaser.GameObjects.Container;

    constructor ()
    {
        super('Game');
    }

    create ()
    {
        this.initInputEvent();
        this.camera = this.cameras.main;
        this.camera.setBackgroundColor(0x00ff00);

        // init layers
        this.playerLayer = this.add.container();

        this.background = this.add.image(0, 0, 'game-ground');

        this.player = new Tank(this.matter.world, (this.game.config.width as number)/2, (this.game.config.height as number)/2, 'star');
        this.playerLayer.add(this.player);

        EventBus.emit('current-scene-ready', this);

        console.log(this.matter.world);
    }

    initInputEvent(){
        this.input.keyboard?.on('keydown',(event:any)=>{
            
            this.moveGround(event.key);
            let {height, width, x, y} = this.background;
            console.log('bg info: ', width, height, x, y);
            

        });
    }

    moveGround(key:String){
        let {width, height, x, y} =this.background;
        const canMoveLeft = x < (width/2);
        const canMoveRight = x > (this.game.config.width as number) - (width/2);
        const canMoveUp = y < (height/2);
        const canMoveDown = y > (this.game.config.height as number) - (height/2);


        if(key === "ArrowLeft" && canMoveLeft){
            this.background.x += 2;
        }
        if(key === "ArrowRight" && canMoveRight){
            this.background.x -= 2;
        }
        if(key === "ArrowUp" && canMoveUp){
            this.background.y += 2;
        }
        if(key === "ArrowDown" && canMoveDown){
            this.background.y -= 2;
        }
    }

    changeScene ()
    {
        this.scene.start('GameOver');
    }
}
