import {Physics } from 'phaser';
export default class Tank extends Phaser.Physics.Matter.Sprite {

    private movePath: Phaser.Math.Vector2[] = []
	private moveToTarget?: Phaser.Math.Vector2


    moveAlong(path: Phaser.Math.Vector2[])
	{
		if (!path || path.length <= 0)
		{
			return
		}

		this.movePath = path
		this.moveTo(this.movePath.shift()!)
	}

	moveTo(target: Phaser.Math.Vector2)
	{
		this.moveToTarget = target;
	}

    update(){
        let dx = 0
        let dy = 0

        if (this.moveToTarget)
        {
            dx = this.moveToTarget.x - this.x
            dy = this.moveToTarget.y - this.y

            if (Math.abs(dx) < 5)
            {
                dx = 0
            }
            if (Math.abs(dy) < 5)
            {
                dy = 0
            }

            if (dx === 0 && dy === 0)
            {
                if (this.movePath.length > 0)
                {
                    this.moveTo(this.movePath.shift()!)
                    return
                }
                
                this.moveToTarget = undefined
            }
        }

        // this logic is the same except we determine
        // if a key is down based on dx and dy
        const leftDown = dx < 0
        const rightDown = dx > 0
        const upDown = dy < 0
        const downDown = dy > 0

        const speed = 100

        if (leftDown)
        {
            //this.anims.play('faune-run-side', true)
            this.setVelocity(-speed, 0)

            this.flipX = true
        }
        else if (rightDown)
        {
            //this.anims.play('faune-run-side', true)
            this.setVelocity(speed, 0)

            this.flipX = false
        }
        else if (upDown)
        {
            //this.anims.play('faune-run-up', true)
            this.setVelocity(0, -speed)
        }
        else if (downDown)
        {
            //this.anims.play('faune-run-down', true)
            this.setVelocity(0, speed)
        }
        else
        {
            // const parts = this.anims.currentAnim.key.split('-')
            // parts[1] = 'idle'
            //this.anims.play(parts.join('-'))
            this.setVelocity(0, 0)
        }
    }
}